package com.wooqer.videosearch.manager;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.wooqer.videosearch.R;
import com.wooqer.videosearch.models.CommonVo;
import com.wooqer.videosearch.models.VimeoSearchVo;
import com.wooqer.videosearch.models.YoutubeSearchVo;
import com.wooqer.videosearch.network.ApiServices;
import com.wooqer.videosearch.network.RestClient;
import com.wooqer.videosearch.utils.Constants;
import com.wooqer.videosearch.utils.DialogUtil;
import com.wooqer.videosearch.utils.NetworkManager;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Devale on 23-08-2015.
 */

/**
 * Singleton class to handle all the network calls related to video search
 */
public class VideoSearchManager extends BaseManager {

    private NotifyAdapter mNotifyAdapter;
    private static VideoSearchManager mVideoSearchManager;

    private ApiServices mYoutubeApiServices;
    private ApiServices mVimeoApiServices;

    private Dialog mSearchDialog;

    private List<CommonVo> mCommonVoList;
    private static int mSearchedKeyLength = 10;
    public static String[] mSearchedKey = new String[mSearchedKeyLength];
    private static int marker = 0;

    private String mNextYoutubePageToken = "";
    private int mCurrentVimeoPage = 0;
    private boolean isYoutubeSearchLoading = false;
    private boolean isVimeoSearchLoading = false;

    public VideoSearchManager() {
        mYoutubeApiServices = new RestClient(true).getApiServices();
        mVimeoApiServices = new RestClient(false).getApiServices();

        mCommonVoList = new ArrayList<>();
        for (int i = 0; i < mSearchedKey.length; i++) {
            mSearchedKey[i] = "";
        }

    }

    public static VideoSearchManager newInstance() {
        if (mVideoSearchManager == null)
            mVideoSearchManager = new VideoSearchManager();

        return mVideoSearchManager;
    }


    public void searchVideos(String query, int networkStatus, Context context) {
        DialogUtil dialogUtil = new DialogUtil();
        mSearchDialog = dialogUtil.showDialog(context, context.getString(R.string.loading_search_results));

        if (networkStatus == NetworkManager.TYPE_NOT_CONNECTED) {
            getSavedDataForQuery(query);

            mSearchDialog.dismiss();
            Toast.makeText(context, R.string.no_network_available, Toast.LENGTH_SHORT).show();
        } else {

            mCommonVoList.clear();

            mNextYoutubePageToken = "";
            mCurrentVimeoPage = 0;

            if (!isYoutubeSearchLoading)
                searchInYoutube(query);
            if (!isVimeoSearchLoading)
                searchInVimeo(query);
        }
    }

    public void nextPageVideos(String query) {
        if (!isYoutubeSearchLoading)
            searchInYoutube(query);
        if (!isVimeoSearchLoading)
            searchInVimeo(query);
    }

    private void searchInYoutube(final String query) {
        isYoutubeSearchLoading = true;

        mYoutubeApiServices.searchInYoutube(Constants.YOUTUBE_KEY, query, 25, mNextYoutubePageToken, new Callback<YoutubeSearchVo>() {
            @Override
            public void success(YoutubeSearchVo youtubeSearchVo, Response response) {
                if (mSearchDialog != null) {
                    mSearchDialog.dismiss();
                }

                if (youtubeSearchVo != null) {
                    makeCommonList(query, youtubeSearchVo);
                    mNextYoutubePageToken = youtubeSearchVo.nextPageToken;
                    isYoutubeSearchLoading = false;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                isYoutubeSearchLoading = false;
                if (mSearchDialog != null) {
                    mSearchDialog.dismiss();
                }
            }
        });
    }


    private void searchInVimeo(final String query) {
        isVimeoSearchLoading = true;

        mVimeoApiServices.serchInVimeo(Constants.VIMEO_KEY, query, mCurrentVimeoPage + 1, new Callback<VimeoSearchVo>() {
            @Override
            public void success(VimeoSearchVo vimeoSearchVo, Response response) {
                if (vimeoSearchVo != null) {
                    makeCommonList(query, vimeoSearchVo);
                    mCurrentVimeoPage = vimeoSearchVo.page;
                    isVimeoSearchLoading = false;
                }

            }

            @Override
            public void failure(RetrofitError error) {
                isVimeoSearchLoading = false;
            }
        });
    }

    private void makeCommonList(String query, YoutubeSearchVo youtubeSearchVo) {
        CommonVo commonVo;
        for (int i = 0; i < youtubeSearchVo.items.length; i++) {
            commonVo = new CommonVo();
            try {
                YoutubeSearchVo.Items item = youtubeSearchVo.items[i];
                commonVo.isYoutube = true;
                commonVo.vid = item.id.videoId;
                commonVo.title = item.snippet.title;

                if (item.snippet.thumbnails.medium != null)
                    commonVo.thumbnailUrl = item.snippet.thumbnails.medium.url;
                else if (item.snippet.thumbnails.high != null)
                    commonVo.thumbnailUrl = item.snippet.thumbnails.high.url;

                commonVo.query = query;
                commonVo.save();

                mCommonVoList.add(commonVo);


            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        mNotifyAdapter.notifyDataSetChange();

        maintainDbAndKeywords(query);

    }

    private void makeCommonList(String query, VimeoSearchVo vimeoSearchVo) {
        CommonVo commonVo;
        for (int i = 0; i < vimeoSearchVo.data.length; i++) {
            commonVo = new CommonVo();
            try {
                VimeoSearchVo.Data item = vimeoSearchVo.data[i];
                commonVo.isYoutube = false;
                commonVo.vid = item.uri.substring(8);
                commonVo.title = item.name;

                if (item.pictures.sizes[vimeoSearchVo.data[i].pictures.sizes.length - 1] != null && item.pictures.sizes.length > 1)
                    commonVo.thumbnailUrl = item.pictures.sizes[item.pictures.sizes.length - 1].link;
                else if (item.pictures.sizes[vimeoSearchVo.data[i].pictures.sizes.length - 1] != null && item.pictures.sizes.length > 2)
                    commonVo.thumbnailUrl = item.pictures.sizes[item.pictures.sizes.length - 2].link;

                commonVo.query = query;
                commonVo.save();

                mCommonVoList.add(commonVo);

            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        mNotifyAdapter.notifyDataSetChange();

    }

    public List<CommonVo> getCommonVoList() {
        return mCommonVoList;
    }

    private void maintainDbAndKeywords(String query) {
        boolean oneEntryReplaced = false;

        for (int i = 0; i < mSearchedKey.length; i++) {
            if (mSearchedKey[i].equalsIgnoreCase(query)) {
                mSearchedKey[i] = query;
                oneEntryReplaced = true;
                break;
            }
        }

        if (!oneEntryReplaced) {
            if (mSearchedKey[marker] != null) {
                deleteSavedDataForQuery(mSearchedKey[marker]);
                mSearchedKey[marker] = query;
            } else {
                mSearchedKey[marker] = query;
            }
        }

        marker++;
        marker = marker % mSearchedKeyLength;

    }

    public void getSavedDataForQuery(String query) {
        try {

            List<CommonVo> commonVoList = CommonVo.find(CommonVo.class, "query = ?", query);
            mCommonVoList.clear();
            mCommonVoList.addAll(commonVoList);
            mNotifyAdapter.notifyDataSetChange();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void deleteSavedDataForQuery(String query) {
        Log.d("Tony1", "deleted query" + query);
        CommonVo.deleteAll(CommonVo.class, "query = ?", query);
    }

    public void setNotifyDataInterface(NotifyAdapter notifyAdapter) {
        mNotifyAdapter = notifyAdapter;
    }

    /**
     * Interface to inform the adapter about data set change
     */
    public interface NotifyAdapter {
        void notifyDataSetChange();
    }

    @Override
    public void cleanUp() {
        mVideoSearchManager = null;
    }
}
