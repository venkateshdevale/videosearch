package com.wooqer.videosearch.models;

import com.orm.SugarRecord;

/**
 * Created by Venkatesh Devale on 24-08-2015.
 */
public class CommonVo extends SugarRecord<CommonVo> {
    public String query;
    public boolean isYoutube;
    public String vid;
    public String title;
    public String thumbnailUrl;

    public CommonVo() {
    }

    public CommonVo(String query, boolean isYoutube, String vid, String title, String thumbnailUrl) {
        this.query = query;
        this.isYoutube = isYoutube;
        this.vid = vid;
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public boolean isYoutube() {
        return isYoutube;
    }

    public void setIsYoutube(boolean isYoutube) {
        this.isYoutube = isYoutube;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

}
