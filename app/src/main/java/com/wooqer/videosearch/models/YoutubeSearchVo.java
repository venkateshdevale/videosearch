package com.wooqer.videosearch.models;

/**
 * Created by Venkatesh Devale on 23-08-2015.
 */
public class YoutubeSearchVo {

    public String nextPageToken;
    public String prevPageToken;
    public PageInfo pageInfo;
    public Items[] items;

    public class PageInfo {
        public String totalResults;
        public String resultsPerPage;
    }

    public class Items {
        public Id id;
        public Snippet snippet;
    }

    public class Id {
        public String videoId;
    }

    public class Snippet {
        public String title;
        public Thumbnails thumbnails;

    }

    public class Thumbnails {
        public Resolution high;
        public Resolution medium;
    }

    public class Resolution {
        public String url;
    }

}
