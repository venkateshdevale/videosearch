package com.wooqer.videosearch.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Venkatesh Devale on 25-08-2015.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        new NetworkManager(context).getConnectivityStatusString(context);
    }
}